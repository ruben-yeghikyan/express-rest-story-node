const express = require('express')
const bodyParser = require('body-parser')
const app = express()
const port = process.env.port || 3000

let users = [
    {
        username: 'John23',
        age: 23,
        email: 'johnsmith@gmail.com'
    },
    {
        username: 'Dave36',
        age: 36,
        email: 'davegomez@gmail.com'
    },
]

app.use(bodyParser.json())

app.get('/users', (req, res) => {
    res.json(users)
})

app.post('/users', (req, res) => {
    const newUser = req.body
    users.push(newUser)
    res.end(`User ${newUser.username} successfully added!`)
})

app.put('/users', (req, res) => {
    const newUser = req.body
    users = users.map(user => user.username === newUser.username ? newUser : user)
    res.end(`User ${newUser.username} successfully updated!`)
})

app.delete('/users', (req, res) => {
    const newUser = req.body
    const deleteIndex = users.findIndex(user => user.username === newUser.username)

    users.splice(deleteIndex, 1);

    res.end(`User ${newUser.username} successfully deleted!`)
})

app.listen(port, () => {
    console.log('app running on ', port)
})
